COMP3260 - Data Security: Advanced Encryption Standard
======================================================

# Abstract #

This project aims to implement AES using 10 rounds for the encryption method.
The overarching goal of this project is to analyse the avalanche effect by
changing comparing the following:

* The effects of removing one of the core functions of AES during each round
* Altering the input and key by a single bit

# Developers #

* Tyler Haigh - C3182929
* Simon Hartcher - C3185970

# Acknowledgements #

The fundermental underpinnings of AES and its implementation have been taken from
the works of Joan Daemon and Vincent Rijmen and published by NIST. The report
can be found [here](http://www.csrc.nist.gov/publications/fips/fips197/fips-197.pdf)

This project was developed as an assignment for COMP3260 - Data Security at the
University of Newcastle, Callaghan, Australia (2015)

# Developer Notes #

This project has been developed using Microsoft's [DNVM](https://github.com/aspnet/dnvm). To build this project:

* We recommend downloading and installing [Scoop](http://scoop.sh/) for Windows machines, or [Homebrew](http://brew.sh) for Apple Mac
* Download and install DNVM via `scoop install dnvm`, or `brew tap aspnet/dnx` then `brew install dnvm`
* Run `dnvm upgrade`
* Clone our project `git clone https://bitbucket.org/thaigh/comp3260-a2.git`
* Build the project using `dnu build`
* Run the project using `dnx . run`