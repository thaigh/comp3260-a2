﻿/*
 * COMP3260 Assignment 2
 * Tyler Haigh - C3182929
 * Simon Hartcher - C3185790
 */

using Aes.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace Aes
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Error: Invalid number of arguments");
                Console.WriteLine("Expected arguments: <Encrypt/Decrypt Flag> <File>");
                Environment.Exit(0);
            }
            
            try
            {
                var flag = args[0];
                var fileName = args[1];
                var message = String.Empty;
                var key = String.Empty;

                // Get the file inputs
                var absolutePath = Path.GetFullPath(fileName);
                if (File.Exists(absolutePath))
                {
                    using (var stream = new StreamReader(absolutePath))
                    {
                        message = stream.ReadLine();
                        key = stream.ReadLine();
                    }

                    if (message == null || key == null)
                    {
                        Console.WriteLine("Message or Key not found in file");
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Console.WriteLine("The input file does not exist");
                    Environment.Exit(0);
                }

                // Perform AES encryption or decryption
                if (flag == "-e" || flag == "--encrypt")
                {
                    Encryption(message, key);
                }
                else if (flag == "-d" || flag == "--decrypt")
                {
                    Decryption(message, key);
                }
                else
                {
                    Console.WriteLine("Error: Invalid flag.");
                    Console.WriteLine("Expected flags:\n" +
                        "\t Encryt:  -e (or) --encrypt\n" +
                        "\t Decrypt: -d (or) --decrypt\n"
                    );
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }
    
        private static void Encryption(string message, string key)
        {
            Console.WriteLine("ENCRYPTION");
            Console.WriteLine("Plaintext P: {0}", message);
            Console.WriteLine("Key K: {0}", key);

            // Round Encryptions will be used to track the changes in the bits after each round
            List<string> roundEncryptions;

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var cipherText = AesAlg.Encrypt(message, key, out roundEncryptions);
            stopwatch.Stop();

            Console.WriteLine("Ciphertext C: {0}", cipherText);
            Console.WriteLine("Running time: {0}", stopwatch.Elapsed);

            Console.WriteLine("Avalanche:");
            var avalancheResults = new Dictionary<int, List<List<RoundResult>>>(5);
            
            //AES0->AES4
            for (var i = 0; i < 5; i++)
            {
                avalancheResults[i] = new List<List<RoundResult>>();

                //Add original message
                string original = AesAlg.Encrypt(message, key, out roundEncryptions, i);
                avalancheResults[i].Add(roundEncryptions.Select(r => new RoundResult { CipherText = r }).ToList());

                //flip all 128 bits
                for (var j = 0; j < message.Length; j++)
                {
                    var newMessage = (message[j] == '1') ?
                        message.ReplaceAt(j, '0') :
                        message.ReplaceAt(j, '1');

                    // Aes under New Message and Original Key
                    AesAlg.Encrypt(newMessage, key, out roundEncryptions, i);

                    // calc different bits

                    avalancheResults[i].Add(roundEncryptions
                        .Select(r => new RoundResult { CipherText = r, DifferentBits = CalculateDifferentBits(original, r) }).ToList()
                        );
                }
            }

            Console.WriteLine("P and Pi under K");
            PrintAverages(avalancheResults);

            //AES0->AES4
            for (var i = 0; i < 5; i++)
            {
                avalancheResults[i] = new List<List<RoundResult>>();

                //Add original message
                string original = AesAlg.Encrypt(message, key, out roundEncryptions, i);
                avalancheResults[i].Add(roundEncryptions.Select(r => new RoundResult { CipherText = r }).ToList());

                //flip all 128 bits
                for (var j = 0; j < message.Length; j++)
                {
                    var newKey = (key[j] == '1') ?
                        key.ReplaceAt(j, '0') :
                        key.ReplaceAt(j, '1');

                    // Aes under original Message and new Key
                    AesAlg.Encrypt(message, newKey, out roundEncryptions, i);

                    // calc different bits

                    avalancheResults[i].Add(roundEncryptions
                        .Select(r => new RoundResult { CipherText = r, DifferentBits = CalculateDifferentBits(original, r) }).ToList()
                        );
                }
            }

            Console.WriteLine("P under K and Ki");
            PrintAverages(avalancheResults);
        }

        private static void PrintAverages(Dictionary<int, List<List<RoundResult>>> avalancheResults)
        {
            //get the average change per AESi
            var averageChanges = new int[5, 11];
            for (int i = 0; i < 5; i++)
            {
                //AESi
                for (int j = 1; j < 12; j++)
                {
                    averageChanges[i, j - 1] = (int) Math.Round(avalancheResults[i][j].Average(r => r.DifferentBits), 0);
                }
            }

            Console.WriteLine("Round \t AES0 \t AES1 \t AES2 \t AES3 \t AES4");
            for (int i = 0; i < averageChanges.GetLength(1); i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", i
                    , averageChanges[0, i], averageChanges[1, i], averageChanges[2, i]
                    , averageChanges[3, i], averageChanges[4, i]);
            }
            Console.WriteLine();
        }


        private static int CalculateDifferentBits(string a, string b)
        {
            int bitsDifferent = 0;
            for (int i = 0; i < a.Length; i++)
            {
                char left = a[i];
                char right = b[i];
                if (left != right)
                    bitsDifferent++;
            }
            return bitsDifferent;
        }

        private static void Decryption(string cipherText, string key)
        {
            var message = AesAlg.Decrypt(cipherText, key);

            Console.WriteLine("DECRYPTION");
            Console.WriteLine("Ciphertext C: {0}", cipherText);
            Console.WriteLine("Key K: {0}", key);
            Console.WriteLine("Plaintext C: {0}", message);
        }
    }

    public class RoundResult
    {
        public string CipherText { get; set; }
        public int DifferentBits { get; set; }
    }
}
