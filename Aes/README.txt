﻿COMP3260 Assignment 2
Tyler Haigh - C3182929
Simon Hartcher - C3185790

Classes
=======

Program:
	This is the entry point for the program that executes the AES encryption/decryption

	Usage: 
		Aes.exe <option> <file>

	Options:
		-e/--encrypt	Encrypts the input file and prints to standard output
		-d/--decrypt	Decrypts the input file and prints to standard output

AesAlg:
	Contains the methods used to implement the Advanced Encryption Standard (AES) (Rijndael)

Extensions:
	This class lists any extensions to existing functionality of primitive entities

Galios:
	Contains the Galois Matrixes used for Mix Columns of the AES (Rijndael) algorithm.

RCon:
	Contains the Round Constant Lookup table, used in Key Expansion

SBox:
	Contains the Forward and Inverse SBoxes.

Word:
	A 4-byte Word implementation

