/*
 * COMP3260 Assignment 2
 * Tyler Haigh - C3182929
 * Simon Hartcher - C3185790
 */

using System;
using System.Collections.Generic;

namespace Aes.Core
{

    /// <summary>
    /// Contains the methods used to implement the Advanced Encryption Standard (AES) (Rijndael)
    /// </summary>
	public static class AesAlg
	{
        private const short NumRounds = 10;
        private static short _ByteLength = 2;

        public static string Encrypt(string message, string key, out List<string> roundEncryptions, int skipStep = 0)
		{

            if (message.Length != 128 || key.Length != 128)
                throw new Exception("Invalid message or key length. Must be 128 bits long");

            // Round Encryptions will be used to track the changes in the bits after each round
            roundEncryptions = new List<string>();

            // Get convert the inputs (binary strings) to their byte format
            var messageBytes = GetBytes(message);
            var keyBytes = GetBytes(key);

            // Convert to a matrix
            var messageMatrix = ToMatrix(messageBytes);
            var expandedKeys = KeyExpansion(keyBytes);

            // Apply initial key
            var state = AddRoundKey(messageMatrix, 0, expandedKeys);

            // Run through 9 rounds
            for (var i = 1; i < NumRounds; i++)
            {
                state = ApplyRound(state, i, expandedKeys, true, skipStep);

                // Generate round result
                string roundResult = "";
                for (int j = 0; j < state.GetLength(0); j++)
                {
                    for (int k = 0; k < state.GetLength(1); k++)
                    {
                        string binaryVal = Convert.ToString(state[k, j], 2);
                        roundResult += binaryVal.PadLeft(8, '0');
                    }
                }
                roundEncryptions.Add(roundResult);
              
            }

            // Final Round
            if (skipStep != 1)
                state = SubstituteBytes(state, true);
            if (skipStep != 2)
                state = ShiftRows(state, true);
            if (skipStep != 4)
                state = AddRoundKey(state, NumRounds, expandedKeys);

            // Generate output string
            var encrypted = "";
            for (var j = 0; j < state.GetLength(0); j++)
            {
                for (var k = 0; k < state.GetLength(1); k++)
                {
                    var binaryVal =  Convert.ToString(state[k, j], 2);
                    encrypted += binaryVal.PadLeft(8, '0');
                }
            }
            roundEncryptions.Add(encrypted);

            return encrypted;
		}

        public static string Decrypt(string ciperText, string key, int skipStep = 0)
		{
            if (ciperText.Length != 128 || key.Length != 128)
                throw new Exception("Invalid message or key length. Must be 128 bits long");

            // Get convert the inputs (binary strings) to their byte format
            var messageBytes = GetBytes(ciperText);
            var keyBytes = GetBytes(key);

            // Convert to a matrix
            var messageMatrix = ToMatrix(messageBytes);
            var expandedKeys = KeyExpansion(keyBytes);

            // Apply initial key
            var state = AddRoundKey(messageMatrix, NumRounds, expandedKeys);

            // Run through 9 rounds
            for (var i = NumRounds - 1; i > 0; i--)
            {
                state = ApplyRound(state, i, expandedKeys, false, skipStep);
            }

            // Final Round
            // It was interpreted in the assignment specification that
            // Decryption will not have steps skipped
            state = ShiftRows(state, false);
            state = SubstituteBytes(state, false);
            state = AddRoundKey(state, 0, expandedKeys);

            // Generate output string
            var decrypted = "";
            for (var j = 0; j < state.GetLength(0); j++)
            {
                for (var k = 0; k < state.GetLength(1); k++)
                {
                    var binaryVal = Convert.ToString(state[k, j], 2);
                    decrypted += binaryVal.PadLeft(8, '0');
                }
            }

            return decrypted;

		}

        private static byte[,] ToMatrix(byte[] array)
        {
            var matrixDimension = array.Length / 4;

            var matrix = new byte[matrixDimension, matrixDimension];
            var counter = 0;
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    matrix[j, i] = array[counter++];
                }
            }

            return matrix;
        }
		
        /// <summary>
        /// Gets the byte values from an integer
        /// </summary>
        /// <param name="message">The messenge as an integer</param>
        /// <returns>An array of bytes that represent the integer</returns>
		public static byte[] GetBytes(int message)
		{
			var bytes = BitConverter.GetBytes(message);
			Array.Reverse(bytes); //BitConverter returns in reverse order
			
			return bytes;
		}

        private static byte[] GetBytes(string message)
        {
            var messageByteLength = message.Length / 8;
            var messageBytes = new byte[messageByteLength];
            for (var i = 0; i < messageByteLength; ++i)
            {
                messageBytes[i] = Convert.ToByte(message.Substring(8 * i, 8), 2);
            }
            return messageBytes;
        }
		
        public static byte[,] ApplyRound(byte[,] stateMatrix, int roundNumber, 
            Word[] roundKeys, bool encrypt = true, int skipStep = 0)
        {
            var clone = (byte[,])stateMatrix.Clone();

            if (encrypt)
            {
                if (skipStep != 1)
                    clone = SubstituteBytes(clone, encrypt);
                if (skipStep != 2)
                    clone = ShiftRows(clone, encrypt);
                if (skipStep != 3)
                    clone = MixColumns(clone, encrypt);
                if (skipStep != 4)
                    clone = AddRoundKey(clone, roundNumber, roundKeys);
            }
            else
            {
                // It has been interpreted in the assignment specification that
                // Decryption will not require skipping steps for comparison of
                // the Avalanche Effect
                clone = ShiftRows(clone, encrypt);
                clone = SubstituteBytes(clone, encrypt);
                clone = AddRoundKey(clone, roundNumber, roundKeys);
                clone = MixColumns(clone, encrypt);
            }

            return clone;
        }

        /// <summary>
        /// Substitutes all bytes in the state matrix with those in the respective
        /// SBox depending on whether tasked to encrypt or decrypt
        /// </summary>
        /// <param name="stateMatrix">The current AES state matrix for the message</param>
        /// <param name="encrypt">Whether the algorithm should encrypt the message or not</param>
        /// <returns>A copy of the state matrix with bytes substituted using the appropriate SBox</returns>
		public static byte[,] SubstituteBytes(byte[,] stateMatrix, bool encrypt)
		{
			var clone = (byte[,])stateMatrix.Clone();

            for (var i = 0; i < stateMatrix.GetLength(0); i++)
            {
                for (var j = 0; j < stateMatrix.GetLength(1); j++)
                {
                    clone[i, j] = SBox.Substitute(stateMatrix[i, j], encrypt);
                }
            }

            return clone;
		}
		
        /// <summary>
        /// Performs the Shift Rows method as part of a round in AES.
        /// Shifts rows according the the following specification:
        /// 
        /// <para>Under Encryption</para>
        /// <para> - Row 0 is left unchanged</para>
        /// <para> - Row 1 shifts left by 1 byte</para>
        /// <para> - Row 2 shifts left by 2 bytes</para>
        /// <para> - Row 3 shifts left by 3 bytes</para>
        /// 
        /// <para>Under Decryption</para>
        /// <para> - Row 0 is left unchanged</para>
        /// <para> - Row 1 shifts right by 1 byte</para>
        /// <para> - Row 2 shifts right by 2 bytes</para>
        /// <para> - Row 3 shifts right by 3 bytes</para>
        /// 
        /// </summary>
        /// <param name="matrix">The current AES state matrix for the message</param>
        /// <param name="encrypt">Whether the algorithm should encrypt the message or not</param>
        /// <returns>A copy of the state matrix shifted according to the specification</returns>
		public static byte[,] ShiftRows(byte[,] matrix, bool encrypt = true)
		{
			
			var clone = (byte[,])matrix.Clone();
			
			for (var i = 1; i < clone.GetLength(0); i++)
			{
				
				for (var j = 0; j < clone.GetLength(1); j++)
				{
					
                    // Calculate the shift
                    var shiftFrom = ( (encrypt) ? (j + i) : (j) ) % clone.GetLength(1);
					var shiftTo =   ( (encrypt) ? (j) : (i + j) ) % clone.GetLength(1);
					
					//Console.WriteLine("    From {0} To {1}", shiftFrom, shiftTo);
					
					clone[i,shiftTo] = matrix[i,shiftFrom];
				}
			}
			
			return clone;
		}
		
        /// <summary>
        /// Performs the Mix Columns method as part of a round in AES.
        /// Uses a Galois Field GF(2^8) matrix to perform the multiplication
        /// involved when mixing the columns
        /// </summary>
        /// <param name="stateMatrix">The curret AES state matrix for te message</param>
        /// <param name="encrypt">Whether the algorithm should encrypt the message or not</param>
        /// <returns>A copy of the state matix with the Mix Column applied</returns>
		public static byte[,] MixColumns(byte[,] stateMatrix, bool encrypt = true)
		{
			var clone = (byte[,])stateMatrix.Clone();
			
			for (var i = 0; i < clone.GetLength(0); i++)
			{
				
                // Perform the GF multiplication
                var galoisMatrix = (encrypt) ? Galois.GaloisMatrix : Galois.InverseGaloisMatrix;
                var column = stateMatrix.Column(i);
                var newBytes = Galois.MultiplyByte(column, galoisMatrix);

                // Update the bytes
                clone[0, i] = newBytes[0];
                clone[1, i] = newBytes[1];
                clone[2, i] = newBytes[2];
                clone[3, i] = newBytes[3];
			}
			
			return clone;
		}

        public static byte[,] AddRoundKey(byte[,] stateMatrix, int round, Word[] roundkeys)
		{
			// XOR the state with 128 bits of the round key
            // Decryption is the same due to XOR
            var rk1 = roundkeys[round * 4];
            var rk2 = roundkeys[round * 4 + 1];
            var rk3 = roundkeys[round * 4 + 2];
            var rk4 = roundkeys[round * 4 + 3];

            var roundKeyMatrix = new [,]
            {
                {rk1.Bytes[0], rk2.Bytes[0], rk3.Bytes[0], rk4.Bytes[0]},
                {rk1.Bytes[1], rk2.Bytes[1], rk3.Bytes[1], rk4.Bytes[1]},
                {rk1.Bytes[2], rk2.Bytes[2], rk3.Bytes[2], rk4.Bytes[2]},
                {rk1.Bytes[3], rk2.Bytes[3], rk3.Bytes[3], rk4.Bytes[3]}
            };

            var clone = (byte[,])stateMatrix.Clone();

            for (var i = 0; i < stateMatrix.GetLength(0); i++)
            {
                for (var j = 0; j < stateMatrix.GetLength(1); j++)
                {
                    clone[i, j] = (byte)(stateMatrix[i, j] ^ roundKeyMatrix[i, j]);
                }
            }

            return clone;

		}

        public static Word[] KeyExpansion(byte[] key)
        {
            var expanded = new Word[44];
            Word temp;

            // Copy the key
            for (var i = 0; i < 4; i++)
            {
                var w = new Word(
                      key[4 * i]
                    , key[ (4 * i) + 1]
                    , key[ (4 * i) + 2]
                    , key[ (4 * i) + 3]
                );

                expanded[i] = w;
            }

            // Expand the Key
            for (var i = 4; i < 44; i++)
            {
                // Create a clone of the word
                temp = (Word)expanded[i - 1].Clone();

                var newWord = KeyGeneration(temp, i, expanded[i - 4]);

                //Add to expanded
                expanded[i] = newWord;
            }

            return expanded;
        }

        /// <summary>
        /// Generates a new Key as a Word.
        /// <para>Contains the auxiliary function used when the key index is
        /// a multiple of four
        /// </para>
        /// </summary>
        /// <param name="word">The original key as a Word</param>
        /// <param name="keyIndex">The index of the generated key in the expanded list</param>
        /// <param name="previousWord">The word at index (keyIndex - 4)</param>
        /// <returns></returns>
        public static Word KeyGeneration(Word word, int keyIndex, Word previousWord)
        {

            // Perform the auxiliary function if the key is a multiple of 4
            if (keyIndex % 4 == 0)
            {
                // Rotate the Word
                word.Rotate();

                // Substitute the Bytes
                for (var i = 0; i < 4; i++)
                {
                    word.Bytes[i] = SBox.Substitute(word.Bytes[i]);
                }

                // XOR with Round Constant
                var constant = new Word();
                constant.Bytes[0] = RCon.Constants[0, keyIndex / 4];

                for (var j = 0; j < 4; j++)
                {
                    word.Bytes[j] = (byte)(word.Bytes[j] ^ constant.Bytes[j]);
                }
            }

            // Final XOR. Not part of auxiliary function,
            // but is used in final step to calculate a new key
            Word newWord = new Word();
            for (var k = 0; k < 4; k++)
            {
                newWord.Bytes[k] = (byte)(previousWord.Bytes[k] ^ word.Bytes[k]);
            }

            return newWord;

        }
	}
}