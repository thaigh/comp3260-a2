﻿/*
 * COMP3260 Assignment 2
 * Tyler Haigh - C3182929
 * Simon Hartcher - C3185790
 */

using System;

namespace Aes.Core
{
    /// <summary>
    /// Represents a 4-Byte word
    /// </summary>
    public class Word : ICloneable
    {
        /// <summary>
        /// Gets or Sets the bytes in the Word
        /// </summary>
        public byte[] Bytes { get; private set; }

        /// <summary>
        /// Default Constructor for the Word. Initialises the byte array
        /// </summary>
        public Word()
        {
            Bytes = new byte[4];
        }

        /// <summary>
        /// Constructor for a Word with initial Byte values
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public Word(byte a, byte b, byte c, byte d)
        {
            Bytes = new byte[4];
            Bytes[0] = a;
            Bytes[1] = b;
            Bytes[2] = c;
            Bytes[3] = d;
        }

        public object Clone()
        {
            var clone = new Word();
            clone.Bytes[0] = this.Bytes[0];
            clone.Bytes[1] = this.Bytes[1];
            clone.Bytes[2] = this.Bytes[2];
            clone.Bytes[3] = this.Bytes[3];
            return clone;
        }

        /// <summary>
        /// Rotates the bytes in the word by one index according to the direction specified
        /// </summary>
        /// <param name="left">Whether to rotate the bytes left or not</param>
        public void Rotate(bool left = true)
        {
            byte temp;
            
            if (left)
            {
                // Rotate bytes left one index. Store the first byte in temp
                temp = Bytes[0];
                Bytes[0] = Bytes[1];
                Bytes[1] = Bytes[2];
                Bytes[2] = Bytes[3];
                Bytes[3] = temp;
            }
            else
            {
                // Rotate bytes right one index. Store the last byte in temp
                temp = Bytes[3];
                Bytes[3] = Bytes[2];
                Bytes[2] = Bytes[1];
                Bytes[1] = Bytes[0];
                Bytes[0] = temp;
            }
        }

        /// <summary>
        /// Returns a string representation of the Word's bytes
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0:X}-{1:X}-{2:X}-{3:X}"
                , Bytes[0], Bytes[1], Bytes[2], Bytes[3]);
        }
    }
}
