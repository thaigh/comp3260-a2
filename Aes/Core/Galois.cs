/*
 * COMP3260 Assignment 2
 * Tyler Haigh - C3182929
 * Simon Hartcher - C3185790
 */

namespace Aes.Core
{
    /// <summary>
    /// Contains the Galois Matrixes used for Mix Columns of the AES (Rijndael) algorithm.
    /// <para>Also contains the necessary functions for multiplying bytes when working with matrices</para>
    /// </summary>
	public static class Galois
	{
        /// <summary>
        /// The standard Forward Mix Column transformation matrix
        /// </summary>
        public readonly static byte[,] GaloisMatrix = 
		{
			{0x02, 0x03, 0x01, 0x01},
			{0x01, 0x02, 0x03, 0x01},
			{0x01, 0x01, 0x02, 0x03},
			{0x03, 0x01, 0x01, 0x02}
		};

        /// <summary>
        /// The Inverse Mix Column transformation matrix
        /// </summary>
        public readonly static byte[,] InverseGaloisMatrix = 
		{
			{0x0e, 0x0b, 0x0d, 0x09},
			{0x09, 0x0e, 0x0b, 0x0d},
			{0x0d, 0x09, 0x0e, 0x0b},
			{0x0b, 0x0d, 0x09, 0x0e}
		};
		
        /// <summary>
        /// Multiplies two bytes in GF(2^8)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>(a * b) GF(2^8)</returns>
        private static byte GaloisMultiplyBytes(byte a, byte b)
		{
			byte multiplied = 0;
            const byte maxBitMask = 0x80; // 1000 0000 = x^7
            const byte minBitMask = 0x01; // 0000 0001 = 1
			
			for (var i = 0; i < 8; i++)
			{
				// Perform binary AND to check if last bit is a 0
                if ((b & minBitMask) != 0)
					// Binary XOR
                    multiplied ^= a;
				
                // Perform binary AND to get max bit value
				var maxBit = (byte)(a & maxBitMask);
				
                //Shift a to the left
                a = (byte)(a << 1);

                // Check is max bit is a 1
				if (maxBit != 0)
					// Perform binary XOR in GF(2^8)
                    a ^= 0x1b; // 0001 1011 = x^4 + x^3 + x + 1
				
                //Shift b to the right
				b = (byte)(b >> 1);
			}
			
			return multiplied;
		}

        /// <summary>
        /// Performs a matrix multiplication in GF(2^8) for a matrix column (vector) and a Galois matrix
        /// </summary>
        /// <param name="matrixColumn">The vector/column/array from a matrix to multiply</param>
        /// <param name="galoisMatrix">The Galois Matrix used in the multiplication</param>
        /// <returns>A scaled vector of the original column multplied by the Galois Matrix</returns>
        public static byte[] MultiplyByte(byte[] matrixColumn, byte[,] galoisMatrix)
        {
            var clone = (byte[])matrixColumn.Clone();

            // Addition in GF(2^8) is just XOR. So we use ^ to sum up the values

            clone[0] = (byte)(GaloisMultiplyBytes(galoisMatrix[0, 0], matrixColumn[0])
                            ^ GaloisMultiplyBytes(galoisMatrix[0, 1], matrixColumn[1])
                            ^ GaloisMultiplyBytes(galoisMatrix[0, 2], matrixColumn[2])
                            ^ GaloisMultiplyBytes(galoisMatrix[0, 3], matrixColumn[3])
                            );

            clone[1] = (byte)(GaloisMultiplyBytes(galoisMatrix[1, 0], matrixColumn[0])
                            ^ GaloisMultiplyBytes(galoisMatrix[1, 1], matrixColumn[1])
                            ^ GaloisMultiplyBytes(galoisMatrix[1, 2], matrixColumn[2])
                            ^ GaloisMultiplyBytes(galoisMatrix[1, 3], matrixColumn[3])
                            );

            clone[2] = (byte)(GaloisMultiplyBytes(galoisMatrix[2, 0], matrixColumn[0])
                            ^ GaloisMultiplyBytes(galoisMatrix[2, 1], matrixColumn[1])
                            ^ GaloisMultiplyBytes(galoisMatrix[2, 2], matrixColumn[2])
                            ^ GaloisMultiplyBytes(galoisMatrix[2, 3], matrixColumn[3])
                            );

            clone[3] = (byte)(GaloisMultiplyBytes(galoisMatrix[3, 0], matrixColumn[0])
                            ^ GaloisMultiplyBytes(galoisMatrix[3, 1], matrixColumn[1])
                            ^ GaloisMultiplyBytes(galoisMatrix[3, 2], matrixColumn[2])
                            ^ GaloisMultiplyBytes(galoisMatrix[3, 3], matrixColumn[3])
                            );

            return clone;
        }
		
	}
	
}