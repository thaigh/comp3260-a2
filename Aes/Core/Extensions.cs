﻿/*
 * COMP3260 Assignment 2
 * Tyler Haigh - C3182929
 * Simon Hartcher - C3185790
 */

using System;
using System.Text;

namespace Aes.Core
{
    /// <summary>
    /// This class lists any extensions to existing functionality of primitive entities
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Replaces the character in a string at the given index
        /// </summary>
        /// <param name="input">The original string</param>
        /// <param name="index">The index to replace at</param>
        /// <param name="newChar">The new character to replace with</param>
        /// <returns></returns>
        public static string ReplaceAt(this string input, int index, char newChar)
        {
            if (input == null)
                throw new ArgumentNullException("input", "Cannot replace char in a null string");

            if (index > input.Length ||  index < 0)
                throw new IndexOutOfRangeException("Index is out of bounds");

            var builder = new StringBuilder(input);
            builder[index] = newChar;
            return builder.ToString();
        }

        /// <summary>
        /// Gets the specified row from a C# 2-dimensional array (e.g. [,] )
        /// </summary>
        /// <param name="matrix">The C# 2-Dimensional Array</param>
        /// <param name="row">The row to get</param>
        /// <returns>The elements from the matrix in the specified row</returns>
        public static byte[] Row(this byte[,] matrix, int row)
        {
            var rowLength = matrix.GetLength(0);

            // Check that the row is in bounds
            if (row > rowLength)
                throw new IndexOutOfRangeException("Out of Bounds");

            var cloneRow = new byte[rowLength];

            // Copy the row
            for (var i = 0; i < rowLength; i++)
            {
                cloneRow[i] = matrix[row, i];
            }

            return cloneRow;
        }

        /// <summary>
        /// Gets the specified column from a C# 2-dimensional array (e.g. [,] )
        /// </summary>
        /// <param name="matrix">The C# 2-Dimensional Array</param>
        /// <param name="column">The column to get</param>
        /// <returns>The elements from the matrix in the specified column</returns>
        public static byte[] Column(this byte[,] matrix, int column)
        {
            var columnLength = matrix.GetLength(0);

            // Check that the column is in bounds
            if (column > columnLength)
                throw new IndexOutOfRangeException("Out of Bounds");

            var cloneColumn = new byte[columnLength];

            // Copy the column
            for (var i = 0; i < columnLength; i++)
            {
                cloneColumn[i] = matrix[i, column];
            }

            return cloneColumn;
        }
    }
}
