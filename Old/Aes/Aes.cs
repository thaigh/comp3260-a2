using System;

namespace Aes
{
	
	public class Aes
	{
		private static int _NumRounds = 10;
		
		public static void Encrypt(string message)
		{
			
		}
		
		public static int Encrypt(int message)
		{
			byte[] bytes = GetBytes(message);
			return 0;
		}
		
		public static void Decrypt(string ciperText, string key)
		{
			
		}
		
		public static byte[] GetBytes(int message)
		{
			byte[] bytes = BitConverter.GetBytes(message);
			Array.Reverse(bytes); //BitConverter returns in reverse order
			
			//Console.WriteLine("{0} {1} {2} {3}", bytes[0], bytes[1], bytes[2], bytes[3]);
			
			return bytes;
		}
		
		public static void SubstituteBytes()
		{
			
		}
		
		public static byte[,] ShiftRows(byte[,] matrix, bool encrypt = true)
		{
			//Under Encryption:
			//Row 0 is left unchanged
			//Row 1 shifts left by 1 byte
			//Row 2 shifts left by 2 bytes
			//Row 3 shifts left by 3 bytes
			
			//Console.WriteLine( (encrypt) ? "Encrypting..." : "Decrypting..." );
			
			byte[,] clone = (byte[,])matrix.Clone();
			
			for (int i = 1; i < clone.GetLength(0); i++)
			{
				
				//Console.WriteLine("  Shifting Row {0}", i);
				
				for (int j = 0; j < clone.GetLength(1); j++)
				{
					int shiftFrom = ( (encrypt) ? (j + i) : (j) ) % clone.GetLength(1);
					int shiftTo =   ( (encrypt) ? (j) : (i + j) ) % clone.GetLength(1);
					
					//Console.WriteLine("    From {0} To {1}", shiftFrom, shiftTo);
					
					clone[i,shiftTo] = matrix[i,shiftFrom];
				}
			}
			
			return clone;
		}
		
		public static byte[,] MixColumns(byte[,] stateMatrix)
		{
			byte[,] clone = (byte[,])stateMatrix.Clone();
			
			for (int i = 0; i < clone.GetLength(0); i++)
			{
				clone[0,i] = (byte) (Galois.GaloisMultiplyBytes(0x02, stateMatrix[0,i])
						^ Galois.GaloisMultiplyBytes(0x03, stateMatrix[1,i])
						^ stateMatrix[2,i]
						^ stateMatrix[3,i]
						);
				
				clone[1,i] = (byte) (stateMatrix[0,i]
						^ Galois.GaloisMultiplyBytes(0x02, stateMatrix[1,i])
						^ Galois.GaloisMultiplyBytes(0x03, stateMatrix[2,i])
						^ stateMatrix[3,i]
						);
						
				clone[2,i] = (byte) (stateMatrix[0,i]
						^ stateMatrix[1,i]
						^ Galois.GaloisMultiplyBytes(0x02, stateMatrix[2,i])
						^ Galois.GaloisMultiplyBytes(0x03, stateMatrix[3,i])
						);
						
				clone[3,i] = (byte) (Galois.GaloisMultiplyBytes(0x03, stateMatrix[0,i])
						^ stateMatrix[1,i]
						^ stateMatrix[2,i]
						^ Galois.GaloisMultiplyBytes(0x02, stateMatrix[3,i])
						);
			}
			
			return clone;
		}
		
		public static void AddRoundKey()
		{
			
		}
		
	}
}