using System;

namespace Aes
{
	public class Galois
	{
		public static int[,] GaloisMatrix = 
		{
			{02, 03, 01, 01},
			{01, 02, 03, 01},
			{01, 01, 02, 03},
			{03, 01, 01, 02}
		};
		
		public static int[,] InverseGaloisMatrix = 
		{
			{14, 11, 13, 09},
			{09, 14, 11, 13},
			{13, 09, 14, 11},
			{11, 13, 09, 14}
		};
		
		public static byte GaloisMultiplyBytes(byte a, byte b)
		{
			byte multiplied = 0;
			byte hiBitSet;
			
			for (int i = 0; i < 8; i++)
			{
				if ((b & 1) != 0)
					multiplied ^= a;
					
				hiBitSet = (byte)(a & 0x80);
				a <<= 1;
				if (hiBitSet != 0)
					a ^= 0x1b;
				
				b >>= 1;
			}
			
			return multiplied;
		}
		
	}
	
}