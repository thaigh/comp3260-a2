// A Hello World! program in C#. 
using System;
using System.Linq;

public class Program 
{
    static void Main() 
    {
        Console.WriteLine("Hello World!");
        
        var array = new [] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        foreach (int i in array.Where(i => i > 5)) {
            Console.WriteLine(i);
        }
    }
}