using Aes;
using Xunit;
using Xunit.Runner.Dnx;
using Xunit.Sdk;

namespace Tests
{
	public class SBoxTests
	{
		[Fact]
		public static void ForwardSubstitute_95_2A()
		{
			byte reference = 0x95;
			byte expected = 0x2A;
			byte actual = SBox.Substitute(reference, true);
			
			Assert.Equal(expected, actual);
		}
		
		[Fact]
		public static void InverseSubstitute_2A_95()
		{
			byte reference = 0x2A;
			byte expected = 0x95;
			byte actual = SBox.Substitute(reference, false);
			
			Assert.Equal(expected, actual);
		}
		
		[Fact]
		public static void ForwardInverse_00_00()
		{
			//Testing with 0x00 since we have tested the individual
			//substitutions with 0x95 and 0x2A
			byte reference = 0x00;
			byte substituted = SBox.Substitute(reference, true);
			byte actual = SBox.Substitute(substituted, false);
			
			Assert.Equal(reference, actual);
		}
	}
}