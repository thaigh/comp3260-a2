using Aes;
using Xunit;
using Xunit.Runner.Dnx;
using Xunit.Sdk;

namespace Tests

{
	public class AesTests
	{
		[Fact]
		public static void GetBytes_400_00000190()
		{
			int message = 400;
			byte[] expecteds = new byte[] {0x00, 0x00, 0x01, 0x90};
			byte[] actuals = Aes.Aes.GetBytes(message);
			
			Assert.Equal(expecteds.Length, actuals.Length);
			
			for (int i = 0; i < expecteds.Length; i++)
			{
				Assert.Equal(expecteds[i], actuals[i]);
			}
		}
		
		[Fact]
		public static void ShiftRows_Encrypt()
		{
			byte[,] original = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x10, 0x11, 0x12, 0x13},
				{0x20, 0x21, 0x22, 0x23},
				{0x30, 0x31, 0x32, 0x33}
			};
			
			byte[,] expecteds = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x11, 0x12, 0x13, 0x10},
				{0x22, 0x23, 0x20, 0x21},
				{0x33, 0x30, 0x31, 0x32}
			};
			
			byte[,] actuals = Aes.Aes.ShiftRows(original, true);
			
			Assert.Equal(expecteds.Length, actuals.Length);
			
			for (int i = 0; i < expecteds.GetLength(0); i++)
			{
				for (int j = 0; j < expecteds.GetLength(1); j++)
				{
					Assert.Equal(expecteds[i,j], actuals[i,j]);
				}
			}
		}
		
		[Fact]
		public static void ShiftRows_Decrypt()
		{
			byte[,] original = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x10, 0x11, 0x12, 0x13},
				{0x20, 0x21, 0x22, 0x23},
				{0x30, 0x31, 0x32, 0x33}
			};
			
			byte[,] expecteds = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x13, 0x10, 0x11, 0x12},
				{0x22, 0x23, 0x20, 0x21},
				{0x31, 0x32, 0x33, 0x30}
			};
			
			byte[,] actuals = Aes.Aes.ShiftRows(original, false);
			
			Assert.Equal(expecteds.Length, actuals.Length);
			
			for (int i = 0; i < expecteds.GetLength(0); i++)
			{
				for (int j = 0; j < expecteds.GetLength(1); j++)
				{
					Assert.Equal(expecteds[i,j], actuals[i,j]);
				}
			}
		}
		
	}	
}