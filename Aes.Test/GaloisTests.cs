using Aes.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Aes.Test
{
    [TestClass]
	public class GaloisTests
	{
		
		[TestMethod]
        public void MultiplyColumns_GM_0x01010101_0x01010101()
        {
            byte[] original = new byte[] { 0x01, 0x01, 0x01, 0x01 };
            byte[] expecteds = new byte[] { 0x01, 0x01, 0x01, 0x01 };
            byte[] actuals = Galois.MultiplyByte(original, Galois.GaloisMatrix);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void MultiplyColumns_GM_0xDB135345_0x8E4DA1BC()
        {
            byte[] original = new byte[] { 0xdb, 0x13, 0x53, 0x45 };
            byte[] expecteds = new byte[] { 0x8e, 0x4d, 0xa1, 0xbc };
            byte[] actuals = Galois.MultiplyByte(original, Galois.GaloisMatrix);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void MultiplyColumns_GM_0xF20A225C_0x9FDC589D()
        {
            byte[] original = new byte[] { 0xf2, 0x0a, 0x22, 0x5c };
            byte[] expecteds = new byte[] { 0x9f, 0xdc, 0x58, 0x9d };
            byte[] actuals = Galois.MultiplyByte(original, Galois.GaloisMatrix);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void MultiplyColumns_IGM_0x01010101_0x01010101()
        {
            byte[] original = new byte[] { 0x01, 0x01, 0x01, 0x01 };
            byte[] coded = Galois.MultiplyByte(original, Galois.GaloisMatrix);
            byte[] actuals = Galois.MultiplyByte(coded, Galois.InverseGaloisMatrix);

            Assert.AreEqual(original.Length, actuals.Length);

            for (int i = 0; i < original.Length; i++)
            {
                Assert.AreEqual(original[i], actuals[i]);
            }
        }

        [TestMethod]
        public void MultiplyColumns_IGM_0xDB135345_0x8E4DA1BC()
        {
            byte[] original = new byte[] { 0xdb, 0x13, 0x53, 0x45 };
            byte[] coded = Galois.MultiplyByte(original, Galois.GaloisMatrix);
            byte[] actuals = Galois.MultiplyByte(coded, Galois.InverseGaloisMatrix);

            Assert.AreEqual(original.Length, actuals.Length);

            for (int i = 0; i < original.Length; i++)
            {
                Assert.AreEqual(original[i], actuals[i]);
            }
        }

        [TestMethod]
        public void MultiplyColumns_IGM_0xF20A225C_0x9FDC589D()
        {
            byte[] original = new byte[] { 0xf2, 0x0a, 0x22, 0x5c };
            byte[] coded = Galois.MultiplyByte(original, Galois.GaloisMatrix);
            byte[] actuals = Galois.MultiplyByte(coded, Galois.InverseGaloisMatrix);

            Assert.AreEqual(original.Length, actuals.Length);

            for (int i = 0; i < original.Length; i++)
            {
                Assert.AreEqual(original[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetRow_GM_0x02030101()
        {
            byte[] expecteds = new byte[] { 0x02, 0x03, 0x01, 0x01 };
            byte[] actuals = Galois.GaloisMatrix.Row(0);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetRow_GM_0x01020301()
        {
            byte[] expecteds = new byte[] { 0x01, 0x02, 0x03, 0x01 };
            byte[] actuals = Galois.GaloisMatrix.Row(1);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetRow_GM_0x01010203()
        {
            byte[] expecteds = new byte[] { 0x01, 0x01, 0x02, 0x03 };
            byte[] actuals = Galois.GaloisMatrix.Row(2);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetRow_GM_0x03010102()
        {
            byte[] expecteds = new byte[] { 0x03, 0x01, 0x01, 0x02 };
            byte[] actuals = Galois.GaloisMatrix.Row(3);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetColumn_GM_0x02010103()
        {
            byte[] expecteds = new byte[] { 0x02, 0x01, 0x01, 0x03 };
            byte[] actuals = Galois.GaloisMatrix.Column(0);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetColumn_GM_0x03020101()
        {
            byte[] expecteds = new byte[] { 0x03, 0x02, 0x01, 0x01 };
            byte[] actuals = Galois.GaloisMatrix.Column(1);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetColumn_GM_0x01030201()
        {
            byte[] expecteds = new byte[] { 0x01, 0x03, 0x02, 0x01 };
            byte[] actuals = Galois.GaloisMatrix.Column(2);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

        [TestMethod]
        public void GetColumn_GM_0x01010302()
        {
            byte[] expecteds = new byte[] { 0x01, 0x01, 0x03, 0x02 };
            byte[] actuals = Galois.GaloisMatrix.Column(3);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Assert.AreEqual(expecteds[i], actuals[i]);
            }
        }

	}
}