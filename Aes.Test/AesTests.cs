using Aes.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Aes.Test
{
    [TestClass]
	public class AesTests
	{
		[TestMethod]
        public void GetBytes_400_00000190()
		{
			int message = 400;
			byte[] expecteds = new byte[] {0x00, 0x00, 0x01, 0x90};
			byte[] actuals = AesAlg.GetBytes(message);
			
			Assert.AreEqual(expecteds.Length, actuals.Length);
			
			for (int i = 0; i < expecteds.Length; i++)
			{
                Assert.AreEqual(expecteds[i], actuals[i]);
			}
		}
		
        [TestMethod]
        public void ShiftRows_Encrypt()
		{
			byte[,] original = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x10, 0x11, 0x12, 0x13},
				{0x20, 0x21, 0x22, 0x23},
				{0x30, 0x31, 0x32, 0x33}
			};
			
			byte[,] expecteds = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x11, 0x12, 0x13, 0x10},
				{0x22, 0x23, 0x20, 0x21},
				{0x33, 0x30, 0x31, 0x32}
			};
			
			byte[,] actuals = AesAlg.ShiftRows(original, true);

            Assert.AreEqual(expecteds.Length, actuals.Length);
			
			for (int i = 0; i < expecteds.GetLength(0); i++)
			{
				for (int j = 0; j < expecteds.GetLength(1); j++)
				{
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
				}
			}
		}
		
        [TestMethod]
        public void ShiftRows_Decrypt()
		{
			byte[,] original = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x10, 0x11, 0x12, 0x13},
				{0x20, 0x21, 0x22, 0x23},
				{0x30, 0x31, 0x32, 0x33}
			};
			
			byte[,] expecteds = new byte[,] 
			{
				{0x00, 0x01, 0x02, 0x03},
				{0x13, 0x10, 0x11, 0x12},
				{0x22, 0x23, 0x20, 0x21},
				{0x31, 0x32, 0x33, 0x30}
			};
			
			byte[,] actuals = AesAlg.ShiftRows(original, false);

            Assert.AreEqual(expecteds.Length, actuals.Length);
			
			for (int i = 0; i < expecteds.GetLength(0); i++)
			{
				for (int j = 0; j < expecteds.GetLength(1); j++)
				{
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
				}
			}
		}
		
        [TestMethod]
        public void MixColumns_Encrypt()
        {
            byte[,] original = new byte[,] {
                { 0x01, 0xdb, 0xf2, 0x2d },
                { 0x01, 0x13, 0x0a, 0x26 },
                { 0x01, 0x53, 0x22, 0x31 },
                { 0x01, 0x45, 0x5c, 0x4c }
            };

           byte[,] expecteds = new byte[,] {
                { 0x01, 0x8e, 0x9f, 0x4d },
                { 0x01, 0x4d, 0xdc, 0x7e },
                { 0x01, 0xa1, 0x58, 0xbd },
                { 0x01, 0xbc, 0x9d, 0xf8 }
            };

            byte[,] actuals = AesAlg.MixColumns(original, true);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void MixColumns_Decrypt()
        {

            byte[,] original = new byte[,] {
                { 0x01, 0x8e, 0x9f, 0x4d },
                { 0x01, 0x4d, 0xdc, 0x7e },
                { 0x01, 0xa1, 0x58, 0xbd },
                { 0x01, 0xbc, 0x9d, 0xf8 }
            };
            
            byte[,] expecteds = new byte[,] {
                { 0x01, 0xdb, 0xf2, 0x2d },
                { 0x01, 0x13, 0x0a, 0x26 },
                { 0x01, 0x53, 0x22, 0x31 },
                { 0x01, 0x45, 0x5c, 0x4c }
            };

            byte[,] actuals = AesAlg.MixColumns(original, false);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void MixColumns_Encrypt_Textbook()
        {

            byte[,] original = new byte[,] {
                { 0x87, 0xF2, 0x4D, 0x97 },
                { 0x6E, 0x4C, 0x90, 0xEC },
                { 0x46, 0xE7, 0x4A, 0xC3 },
                { 0xA6, 0x8C, 0xD8, 0x95 }
            };

            byte[,] expecteds = new byte[,] {
                { 0x47, 0x40, 0xA3, 0x4C },
                { 0x37, 0xD4, 0x70, 0x9F },
                { 0x94, 0xE4, 0x3A, 0x42 },
                { 0xED, 0xA5, 0xA6, 0xBC }
            };

            byte[,] actuals = AesAlg.MixColumns(original, true);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void KeyGeneration_7F8D292F_AC7766F3()
        {
            Word original = new Word(0x7f, 0x8d, 0x29, 0x2f);
            Word previous = new Word(0xea, 0xd2, 0x73, 0x21);
            int keyIndex = 36;

            Word expected = new Word(0xac, 0x77, 0x66, 0xf3);
            Word actual = AesAlg.KeyGeneration(original, keyIndex, previous);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(expected.Bytes[i], actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyGeneration_AF7F6798_DC9037B0()
        {
            Word word3 = new Word(0xaf, 0x7f, 0x67, 0x98);
            Word word0 = new Word(0x0f, 0x15, 0x71, 0xc9);
            int keyIndex = 4;

            Word word4Expected = new Word(0xdc, 0x90, 0x37, 0xb0);
            Word word4Actual = AesAlg.KeyGeneration(word3, keyIndex, word0);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(word4Expected.Bytes[i], word4Actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyGeneration_388115A7_D2C96BB7()
        {
            Word word7 = new Word(0x38, 0x81, 0x15, 0xa7);
            Word word4 = new Word(0xdc, 0x90, 0x37, 0xb0);
            int keyIndex = 8;

            Word word8Expected = new Word(0xd2, 0xc9, 0x6b, 0xb7);
            Word word8Actual = AesAlg.KeyGeneration(word7, keyIndex, word4);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(word8Expected.Bytes[i], word8Actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyGeneration_E6FFD3C6_C0AFDF39()
        {
            Word word11 = new Word(0xe6, 0xff, 0xd3, 0xc6);
            Word word8 = new Word(0xd2, 0xc9, 0x6b, 0xb7);
            int keyIndex = 12;

            Word word12Expected = new Word(0xc0, 0xaf, 0xdf, 0x39);
            Word word12Actual = AesAlg.KeyGeneration(word11, keyIndex, word8);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(word12Expected.Bytes[i], word12Actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyGeneration_2C5C65F1_A5730E96()
        {
            Word word16 = new Word(0x2c, 0x5c, 0x65, 0xf1);
            Word word13 = new Word(0x89, 0x2f, 0x6b, 0x67);
            int keyIndex = 17;

            Word word17Expected = new Word(0xa5, 0x73, 0x0e, 0x96);
            Word word17Actual = AesAlg.KeyGeneration(word16, keyIndex, word13);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(word17Expected.Bytes[i], word17Actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyGeneration_8C2974BF_83E5EF52()
        {
            Word word25 = new Word(0x8c, 0x29, 0x74, 0xbf);
            Word word22 = new Word(0x0f, 0xcc, 0x9b, 0xed);
            int keyIndex = 26;

            Word word26Expected = new Word(0x83, 0xe5, 0xef, 0x52);
            Word word26Actual = AesAlg.KeyGeneration(word25, keyIndex, word22);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(word26Expected.Bytes[i], word26Actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyGeneration_48264520_F31BA2F7()
        {
            Word word32 = new Word(0x48, 0x26, 0x45, 0x20);
            Word word29 = new Word(0xbb, 0x3d, 0xe7, 0xf7);
            int keyIndex = 33;

            Word word33Expected = new Word(0xf3, 0x1b, 0xa2, 0xd7);
            Word word33Actual = AesAlg.KeyGeneration(word32, keyIndex, word29);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(word33Expected.Bytes[i], word33Actual.Bytes[i]);
            }
        }

        [TestMethod]
        public void KeyExpansion_Textboox()
        {
            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] expecteds = new Word[]
            {
                new Word(0x0f, 0x15, 0x71, 0xc9),
                new Word(0x47, 0xd9, 0xe8, 0x59),
                new Word(0x0c, 0xb7, 0xad, 0xd6),
                new Word(0xaf, 0x7f, 0x67, 0x98),
                
                new Word(0xdc, 0x90, 0x37, 0xb0),
                new Word(0x9b, 0x49, 0xdf, 0xe9),
                new Word(0x97, 0xfe, 0x72, 0x3f),
                new Word(0x38, 0x81, 0x15, 0xa7),

                new Word(0xd2, 0xc9, 0x6b, 0xb7),
                new Word(0x49, 0x80, 0xb4, 0x5e),
                new Word(0xde, 0x7e, 0xc6, 0x61),
                new Word(0xe6, 0xff, 0xd3, 0xc6),

                new Word(0xc0, 0xaf, 0xdf, 0x39),
                new Word(0x89, 0x2f, 0x6b, 0x67),
                new Word(0x57, 0x51, 0xad, 0x06),
                new Word(0xb1, 0xae, 0x7e, 0xc0),

                new Word(0x2c, 0x5c, 0x65, 0xf1),
                new Word(0xa5, 0x73, 0x0e, 0x96),
                new Word(0xf2, 0x22, 0xa3, 0x90),
                new Word(0x43, 0x8c, 0xdd, 0x50),

                new Word(0x58, 0x9d, 0x36, 0xeb),
                new Word(0xfd, 0xee, 0x38, 0x7d),
                new Word(0x0f, 0xcc, 0x9b, 0xed),
                new Word(0x4c, 0x40, 0x46, 0xbd),

                new Word(0x71, 0xc7, 0x4c, 0xc2),
                new Word(0x8c, 0x29, 0x74, 0xbf),
                new Word(0x83, 0xe5, 0xef, 0x52),
                new Word(0xcf, 0xa5, 0xa9, 0xef),

                new Word(0x37, 0x14, 0x93, 0x48),
                new Word(0xbb, 0x3d, 0xe7, 0xf7),
                new Word(0x38, 0xd8, 0x08, 0xa5),
                new Word(0xf7, 0x7d, 0xa1, 0x4a),

                new Word(0x48, 0x26, 0x45, 0x20),
                new Word(0xf3, 0x1b, 0xa2, 0xd7),
                new Word(0xcb, 0xc3, 0xaa, 0x72),
                new Word(0x3c, 0xbe, 0x0b, 0x38),

                new Word(0xfd, 0x0d, 0x42, 0xcb),
                new Word(0x0e, 0x16, 0xe0, 0x1c),
                new Word(0xc5, 0xd5, 0x4a, 0x6e),
                new Word(0xf9, 0x6b, 0x41, 0x56),

                new Word(0xb4, 0x8e, 0xf3, 0x52),
                new Word(0xba, 0x98, 0x13, 0x4e),
                new Word(0x7f, 0x4d, 0x59, 0x20),
                new Word(0x86, 0x26, 0x18, 0x76),
            };

            Word[] actuals = AesAlg.KeyExpansion(key);

            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int i = 0; i < expecteds.Length; i++)
            {
                Word expected = expecteds[i];
                Word actual = actuals[i];

                for (int j = 0; j < 4; j++)
                {
                    Assert.AreEqual(expected.Bytes[j], actual.Bytes[j]);
                }
            }
        }

        [TestMethod]
        public void AddRoundKey()
        {
            byte[,] state = new byte[,]
            {
                { 0x47, 0x40, 0xA3, 0x4C },
                { 0x37, 0xD4, 0x70, 0x9F },
                { 0x94, 0xE4, 0x3A, 0x42 },
                { 0xED, 0xA5, 0xA6, 0xBC }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0xEB, 0x59, 0x8B, 0x1B },
                { 0x40, 0x2E, 0xA1, 0xC3 },
                { 0xF2, 0x38, 0x13, 0x42 },
                { 0x1E, 0x84, 0xE7, 0xD6 }
            };

            Word[] roundKeys = new Word[]
            {
                new Word( 0xAC, 0x77, 0x66, 0xF3 ),
                new Word( 0x19, 0xFA, 0xDC, 0x21 ),
                new Word( 0x28, 0xD1, 0x29, 0x41 ),
                new Word( 0x57, 0x5C, 0x00, 0x6A )
            };

            int round = 0;

            byte[,] actuals = AesAlg.AddRoundKey(state, round, roundKeys);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for(int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }

        }

        [TestMethod]
        public void SubstituteBytes()
        {
            byte[,] state = new byte[,]
            {
                { 0xEA, 0x04, 0x65, 0x85 },
                { 0x83, 0x45, 0x5D, 0x96 },
                { 0x5C, 0x33, 0x98, 0xB0 },
                { 0xF0, 0x2D, 0xAD, 0xC5 }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0x87, 0xF2, 0x4D, 0x97 },
                { 0xEC, 0x6E, 0x4C, 0x90 },
                { 0x4A, 0xC3, 0x46, 0xE7 },
                { 0x8C, 0xD8, 0x95, 0xA6 }
            };

            byte[,] actuals = AesAlg.SubstituteBytes(state, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round1()
        {
            byte[,] state = new byte[,]
            {
                { 0x0e, 0xce, 0xf2, 0xd9 },
                { 0x36, 0x72, 0x6b, 0x2b },
                { 0x34, 0x25, 0x17, 0x55 },
                { 0xae, 0xb6, 0x4e, 0x88 }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0x65, 0x0f, 0xc0, 0x4d },
                { 0x74, 0xc7, 0xe8, 0xd0 },
                { 0x70, 0xff, 0xe8, 0x2a },
                { 0x75, 0x3f, 0xca, 0x9c }
            };

            int roundNumber = 1;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round2()
        {
            byte[,] state = new byte[,]
            {
                { 0x65, 0x0f, 0xc0, 0x4d },
                { 0x74, 0xc7, 0xe8, 0xd0 },
                { 0x70, 0xff, 0xe8, 0x2a },
                { 0x75, 0x3f, 0xca, 0x9c }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0x5c, 0x6b, 0x05, 0xf4 },
                { 0x7b, 0x72, 0xa2, 0x6d },
                { 0xb4, 0x34, 0x31, 0x12 },
                { 0x9a, 0x9b, 0x7f, 0x94 }
            };

            int roundNumber = 2;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round3()
        {
            byte[,] state = new byte[,]
            {
                { 0x5c, 0x6b, 0x05, 0xf4 },
                { 0x7b, 0x72, 0xa2, 0x6d },
                { 0xb4, 0x34, 0x31, 0x12 },
                { 0x9a, 0x9b, 0x7f, 0x94 }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0x71, 0x48, 0x5c, 0x7d },
                { 0x15, 0xdc, 0xda, 0xa9 },
                { 0x26, 0x74, 0xc7, 0xbd },
                { 0x24, 0x7e, 0x22, 0x9c }
            };

            int roundNumber = 3;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round4()
        {
            byte[,] state = new byte[,]
            {
                { 0x71, 0x48, 0x5c, 0x7d },
                { 0x15, 0xdc, 0xda, 0xa9 },
                { 0x26, 0x74, 0xc7, 0xbd },
                { 0x24, 0x7e, 0x22, 0x9c }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0xf8, 0xb4, 0x0c, 0x4c },
                { 0x67, 0x37, 0x24, 0xff },
                { 0xae, 0xa5, 0xc1, 0xea },
                { 0xe8, 0x21, 0x97, 0xbc }
            };

            int roundNumber = 4;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round5()
        {
            byte[,] state = new byte[,]
            {
                { 0xf8, 0xb4, 0x0c, 0x4c },
                { 0x67, 0x37, 0x24, 0xff },
                { 0xae, 0xa5, 0xc1, 0xea },
                { 0xe8, 0x21, 0x97, 0xbc }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0x72, 0xba, 0xcb, 0x04 },
                { 0x1e, 0x06, 0xd4, 0xfa },
                { 0xb2, 0x20, 0xbc, 0x65 },
                { 0x00, 0x6d, 0xe7, 0x4e }
            };

            int roundNumber = 5;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round6()
        {
            byte[,] state = new byte[,]
            {
                { 0x72, 0xba, 0xcb, 0x04 },
                { 0x1e, 0x06, 0xd4, 0xfa },
                { 0xb2, 0x20, 0xbc, 0x65 },
                { 0x00, 0x6d, 0xe7, 0x4e }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0x0a, 0x89, 0xc1, 0x85 },
                { 0xd9, 0xf9, 0xc5, 0xe5 },
                { 0xd8, 0xf7, 0xf7, 0xfb },
                { 0x56, 0x7b, 0x11, 0x14 }
            };

            int roundNumber = 6;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round7()
        {
            byte[,] state = new byte[,]
            {
                { 0x0a, 0x89, 0xc1, 0x85 },
                { 0xd9, 0xf9, 0xc5, 0xe5 },
                { 0xd8, 0xf7, 0xf7, 0xfb },
                { 0x56, 0x7b, 0x11, 0x14 }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0xdb, 0xa1, 0xf8, 0x77 },
                { 0x18, 0x6d, 0x8b, 0xba },
                { 0xa8, 0x30, 0x08, 0x4e },
                { 0xff, 0xd5, 0xd7, 0xaa }
            };

            int roundNumber = 7;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round8()
        {
            byte[,] state = new byte[,]
            {
                { 0xdb, 0xa1, 0xf8, 0x77 },
                { 0x18, 0x6d, 0x8b, 0xba },
                { 0xa8, 0x30, 0x08, 0x4e },
                { 0xff, 0xd5, 0xd7, 0xaa }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0xf9, 0xe9, 0x8f, 0x2b },
                { 0x1b, 0x34, 0x2f, 0x08 },
                { 0x4f, 0xc9, 0x85, 0x49 },
                { 0xbf, 0xbf, 0x81, 0x89 }
            };

            int roundNumber = 8;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void ApplyRound_Textbook_Round9()
        {
            byte[,] state = new byte[,]
            {
                { 0xf9, 0xe9, 0x8f, 0x2b },
                { 0x1b, 0x34, 0x2f, 0x08 },
                { 0x4f, 0xc9, 0x85, 0x49 },
                { 0xbf, 0xbf, 0x81, 0x89 }
            };

            byte[,] expecteds = new byte[,]
            {
                { 0xcc, 0x3e, 0xff, 0x3b },
                { 0xa1, 0x67, 0x59, 0xaf },
                { 0x04, 0x85, 0x02, 0xaa },
                { 0xa1, 0x00, 0x5f, 0x34 }
            };

            int roundNumber = 9;

            byte[] key = new byte[]
            {
                0x0f, 0x15, 0x71, 0xc9,
                0x47, 0xd9, 0xe8, 0x59,
                0x0c, 0xb7, 0xad, 0xd6,
                0xaf, 0x7f, 0x67, 0x98
            };

            Word[] roundKeys = AesAlg.KeyExpansion(key);

            byte[,] actuals = AesAlg.ApplyRound(state, roundNumber, roundKeys, true);

            Assert.AreEqual(expecteds.GetLength(0), actuals.GetLength(0));
            Assert.AreEqual(expecteds.GetLength(1), actuals.GetLength(1));

            for (int i = 0; i < expecteds.GetLength(0); i++)
            {
                for (int j = 0; j < expecteds.GetLength(1); j++)
                {
                    Assert.AreEqual(expecteds[i, j], actuals[i, j]);
                }
            }
        }

        [TestMethod]
        public void FullAes_Textbook_Encrypt()
        {
            string message =
                "00000001" + "00100011" + "01000101" + "01100111" +
                "10001001" + "10101011" + "11001101" + "11101111" +
                "11111110" + "11011100" + "10111010" + "10011000" +
                "01110110" + "01010100" + "00110010" + "00010000";

            string key =
                "00001111" + "00010101" + "01110001" + "11001001" +
                "01000111" + "11011001" + "11101000" + "01011001" +
                "00001100" + "10110111" + "10101101" + "11010110" +
                "10101111" + "01111111" + "01100111" + "10011000";

            string expected =
                "11111111" + "00001011" + "10000100" + "01001010" +
                "00001000" + "01010011" + "10111111" + "01111100" +
                "01101001" + "00110100" + "10101011" + "01000011" +
                "01100100" + "00010100" + "10001111" + "10111001";

            List<string> roundResults;
            string actual = AesAlg.Encrypt(message, key, out roundResults);

            Assert.AreEqual(expected.Length, actual.Length);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FullAes_Textbook_Decrypt()
        {
            string expected =
                "00000001" + "00100011" + "01000101" + "01100111" +
                "10001001" + "10101011" + "11001101" + "11101111" +
                "11111110" + "11011100" + "10111010" + "10011000" +
                "01110110" + "01010100" + "00110010" + "00010000";

            string key =
                "00001111" + "00010101" + "01110001" + "11001001" +
                "01000111" + "11011001" + "11101000" + "01011001" +
                "00001100" + "10110111" + "10101101" + "11010110" +
                "10101111" + "01111111" + "01100111" + "10011000";

            string ciphertext =
                "11111111" + "00001011" + "10000100" + "01001010" +
                "00001000" + "01010011" + "10111111" + "01111100" +
                "01101001" + "00110100" + "10101011" + "01000011" +
                "01100100" + "00010100" + "10001111" + "10111001";

            string actual = AesAlg.Decrypt(ciphertext, key);

            Assert.AreEqual(expected.Length, actual.Length);
            Assert.AreEqual(expected, actual);
        }
    }	
}